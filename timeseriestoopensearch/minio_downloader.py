import json
import logging
import re
from datetime import datetime

import boto3
from botocore.client import Config

from timeseriestoopensearch import settings


class MinioDownloader(object):
    s3_url = settings.S3_URL
    s3_bucket_name = settings.S3_BUCKET_NAME
    aws_access_key = settings.AWS_ACCESS_KEY
    aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY
    input_filename_prefix = settings.INPUT_FILENAME_PREFIX

    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)

        self.s3 = boto3.resource('s3',
                                 endpoint_url=self.s3_url,
                                 aws_access_key_id=self.aws_access_key,
                                 aws_secret_access_key=self.aws_secret_access_key,
                                 config=Config(signature_version='s3v4'),
                                 region_name='us-east-1')

    def download_occupations(self, file_to_download):
        if file_to_download:
            line_counter = 0
            self.log.info('Trying to read file %s from Minio' % file_to_download)
            response = self.s3.Object(self.s3_bucket_name, file_to_download).get()
            body = response['Body']

            self.log.info('Streaming ads from file: %s' % file_to_download)

            for line in body.iter_lines():
                try:
                    decoded_line = line.decode("utf-8")
                    line_counter += 1
                    if line_counter % 1000 == 0:
                        self.log.info('Downloaded %s lines from file: %s' % (line_counter, file_to_download))
                    yield json.loads(decoded_line)
                except Exception as e:
                    self.log.error(f"Error when parsing line {e}")
                    yield None
        else:
            self.log.error('No filename was provided. Can not download the file')

    def find_best_match_file(self):
        regex = '^' + self.input_filename_prefix + '*'

        my_bucket = self.s3.Bucket(self.s3_bucket_name)
        best_match_file = None
        best_match_file_last_modified = None
        time_now = datetime.now()
        for bucket_object in my_bucket.objects.all():
            file_name = str(bucket_object.key)
            self.log.debug(f'Current file: {file_name}')
            if re.match(regex, file_name):
                self.log.debug(f'{file_name} is matching prefix')
                self.log.debug(f'File size is {bucket_object.size}')
                if bucket_object.size > 0 and self._check_is_valid_timestamp(bucket_object, time_now):
                    if not best_match_file:
                        best_match_file = file_name
                        best_match_file_last_modified = bucket_object.last_modified
                    else:
                        self.log.debug(f'Already found a matching file. Check dates...')
                        if bucket_object.last_modified > best_match_file_last_modified:
                            best_match_file = file_name
                            best_match_file_last_modified = bucket_object.last_modified

        if not best_match_file:
            raise FileExistsError(
                'No file could be found according to the regex pattern: %s and a timestamp within %s hours' % (
                    regex, settings.INPUT_FILE_MAX_VALID_HOURS))

        return best_match_file

    def _check_is_valid_timestamp(self, bucket_object, time_now):
        filelastmodified = bucket_object.last_modified
        # Remove timezone so the datetime can be compared to now_time.
        filelastmodified = filelastmodified.replace(tzinfo=None)
        difference = time_now - filelastmodified
        diff_in_hours = difference.total_seconds() / 3600

        is_valid_timestamp = (diff_in_hours <= float(settings.INPUT_FILE_MAX_VALID_HOURS))
        if not is_valid_timestamp:
            self.log.debug('File %s is %s hours old right now and is not valid.' % (bucket_object.key, round(diff_in_hours, 2)))

        return is_valid_timestamp
