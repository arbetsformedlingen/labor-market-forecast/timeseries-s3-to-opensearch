import os

ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER", "admin")
ES_PWD = os.getenv("ES_PWD", "admin")
ES_USE_SSL = os.getenv("ES_USE_SSL", False)
ES_VERIFY_CERTS = os.getenv("ES_VERIFY_CERTS", False)

#
ES_OCCUPATION_PREDICTIONS_ALIAS = os.getenv('ES_OCCUPATION_PREDICTIONS_ALIAS', 'occupation-predictions')

VERSION = '0.0.1'

S3_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME', 'predictions')
AWS_ACCESS_KEY = os.environ.get('AWS_ACCESS_KEY', 'minioadmin')
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY', 'minioadmin')
S3_URL = os.environ.get('S3_URL', 'http://localhost:9000')
INPUT_FILENAME_PREFIX = os.getenv('INPUT_FILENAME_PREFIX', 'occupation_predictions_')
INPUT_FILE_MAX_VALID_HOURS = os.getenv('INPUT_FILE_MAX_VALID_HOURS', '720')

# TODO Fix mappings!
MINIO_TO_OPENSEARCH_MAPPINGS = {
    "settings": {
        "analysis": {
            "char_filter": {
                "mapping_char_filter": {
                    "type": "mapping",
                    "mappings": [
                        "- => _"
                    ]
                }
            },
            "analyzer": {
                "char_filter_analyzer": {
                    "tokenizer": "standard",
                    "char_filter": [
                        "mapping_char_filter"
                    ],
                    "filter": [
                        "lowercase"
                    ]
                }
            }
        }
    },
    "mappings" : {
        "properties" : {
            "education" : {
                "properties" : {
                    "code" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "configuration" : {
                        "properties" : {
                            "code" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "type" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "credits" : {
                        "properties" : {
                            "credits" : {
                                "type" : "long"
                            },
                            "system" : {
                                "properties" : {
                                    "code" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "type" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "description" : {
                        "properties" : {
                            "content" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "lang" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "educationLevel" : {
                        "properties" : {
                            "code" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "type" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "eligibility" : {
                        "properties" : {
                            "additionDescription" : {
                                "properties" : {
                                    "content" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "lang" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "eligibilityDescription" : {
                                "properties" : {
                                    "content" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "lang" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "eligibleForStudentAid" : {
                        "properties" : {
                            "code" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "type" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "expires" : {
                        "type" : "date"
                    },
                    "form" : {
                        "properties" : {
                            "code" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "type" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "identifier" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "isVocational" : {
                        "type" : "boolean"
                    },
                    "lastEdited" : {
                        "type" : "date"
                    },
                    "recommendedPriorKnowledge" : {
                        "properties" : {
                            "content" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "lang" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "resultIsDegree" : {
                        "type" : "boolean"
                    },
                    "subject" : {
                        "properties" : {
                            "code" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "name" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "nameEn" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "type" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "title" : {
                        "properties" : {
                            "content" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "lang" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "education_plan" : {
                "properties" : {
                    "brief" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "course_code" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "course_title" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "course_url" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "courses" : {
                        "properties" : {
                            "course" : {
                                "properties" : {
                                    "code" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "description" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "meta" : {
                                        "properties" : {
                                            "department" : {
                                                "properties" : {
                                                    "id" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "label" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "educationCode" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "educationCredits" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "educationLevel" : {
                                                "properties" : {
                                                    "id" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "label" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "educationSubjectGroups" : {
                                                "properties" : {
                                                    "id" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "label" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "educationSubjects" : {
                                                "properties" : {
                                                    "id" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "label" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "educationType" : {
                                                "properties" : {
                                                    "id" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "label" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "forced" : {
                                                "type" : "boolean"
                                            },
                                            "forcedReason" : {
                                                "properties" : {
                                                    "id" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "label" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "forcedSemester" : {
                                                "properties" : {
                                                    "id" : {
                                                        "type" : "long"
                                                    },
                                                    "name" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "seasonName" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "seasonShortName" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "startDate" : {
                                                        "type" : "date"
                                                    },
                                                    "stopDate" : {
                                                        "type" : "date"
                                                    },
                                                    "year" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "id" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "key" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "noOfEvents" : {
                                                "type" : "long"
                                            },
                                            "nyaKeywords" : {
                                                "properties" : {
                                                    "id" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "words" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    },
                                                    "wordsEn" : {
                                                        "type" : "text",
                                                        "fields" : {
                                                            "keyword" : {
                                                                "type" : "keyword",
                                                                "ignore_above" : 256
                                                            }
                                                        }
                                                    }
                                                }
                                            },
                                            "title" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "url" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "validForPostFilter" : {
                                                "type" : "boolean"
                                            }
                                        }
                                    },
                                    "title" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "course_code" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "course_title" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "course_url" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "description" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "title" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "description" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "details" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "labor_market" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "meta" : {
                        "properties" : {
                            "department" : {
                                "properties" : {
                                    "id" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "label" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "educationCode" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "educationCredits" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "educationLevel" : {
                                "properties" : {
                                    "id" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "label" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "educationSubjectGroups" : {
                                "properties" : {
                                    "id" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "label" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "educationSubjects" : {
                                "properties" : {
                                    "id" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "label" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "educationType" : {
                                "properties" : {
                                    "id" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "label" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "events" : {
                                "properties" : {
                                    "eventAdmission" : {
                                        "properties" : {
                                            "admissionStart" : {
                                                "type" : "date"
                                            },
                                            "admissionStop" : {
                                                "type" : "date"
                                            },
                                            "status" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "eventCode" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "eventCompulsoryMeetings" : {
                                        "type" : "long"
                                    },
                                    "eventLanguages" : {
                                        "properties" : {
                                            "id" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "label" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "eventOpenForInternationalStudents" : {
                                        "type" : "boolean"
                                    },
                                    "eventOpenForLateApplication" : {
                                        "type" : "boolean"
                                    },
                                    "eventSemester" : {
                                        "properties" : {
                                            "id" : {
                                                "type" : "long"
                                            },
                                            "name" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "seasonName" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "seasonShortName" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "startDate" : {
                                                "type" : "date"
                                            },
                                            "stopDate" : {
                                                "type" : "date"
                                            },
                                            "year" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "eventStartDate" : {
                                        "type" : "date"
                                    },
                                    "eventStartDateDDMMYYYY" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "eventStartPeriod" : {
                                        "type" : "long"
                                    },
                                    "eventStartWeek" : {
                                        "type" : "long"
                                    },
                                    "eventStopDate" : {
                                        "type" : "date"
                                    },
                                    "eventStopDateDDMMYYYY" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "eventStopWeek" : {
                                        "type" : "long"
                                    },
                                    "eventStudyForm" : {
                                        "properties" : {
                                            "id" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "label" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "eventStudyPace" : {
                                        "type" : "long"
                                    },
                                    "eventStudyTime" : {
                                        "properties" : {
                                            "id" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            },
                                            "label" : {
                                                "type" : "text",
                                                "fields" : {
                                                    "keyword" : {
                                                        "type" : "keyword",
                                                        "ignore_above" : 256
                                                    }
                                                }
                                            }
                                        }
                                    },
                                    "eventUrl" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "validForPostFilter" : {
                                        "type" : "boolean"
                                    }
                                }
                            },
                            "forced" : {
                                "type" : "boolean"
                            },
                            "forcedReason" : {
                                "properties" : {
                                    "id" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "label" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "forcedSemester" : {
                                "properties" : {
                                    "id" : {
                                        "type" : "long"
                                    },
                                    "name" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "seasonName" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "seasonShortName" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "startDate" : {
                                        "type" : "date"
                                    },
                                    "stopDate" : {
                                        "type" : "date"
                                    },
                                    "year" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "id" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "key" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "noOfEvents" : {
                                "type" : "long"
                            },
                            "nyaKeywords" : {
                                "properties" : {
                                    "id" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "words" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "wordsEn" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "title" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "url" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "validForPostFilter" : {
                                "type" : "boolean"
                            }
                        }
                    },
                    "program_code" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "program_title" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "program_url" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "title" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    }
                }
            },
            "education_providers" : {
                "properties" : {
                    "contactAddress" : {
                        "properties" : {
                            "country" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "postBox" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "postCode" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "streetAddress" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "town" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "emailAddress" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "expires" : {
                        "type" : "date"
                    },
                    "identifier" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "name" : {
                        "properties" : {
                            "content" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "lang" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "phone" : {
                        "properties" : {
                            "function" : {
                                "properties" : {
                                    "content" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "lang" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "number" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "responsibleBody" : {
                        "properties" : {
                            "content" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "lang" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "responsibleBodyType" : {
                        "type" : "object"
                    },
                    "urls" : {
                        "properties" : {
                            "content" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "lang" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "visitAddress" : {
                        "properties" : {
                            "country" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "municipalityCode" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "postCode" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "streetAddress" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "town" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "year" : {
                        "properties" : {
                            "code" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "type" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "eventSummary" : {
                "properties" : {
                    "distance" : {
                        "type" : "boolean"
                    },
                    "languageOfInstruction" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "municipalityCode" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "paceOfStudyPercentage" : {
                        "type" : "long"
                    },
                    "timeOfStudy" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    }
                }
            },
            "events" : {
                "properties" : {
                    "application" : {
                        "properties" : {
                            "code" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "first" : {
                                "type" : "date"
                            },
                            "instruction" : {
                                "properties" : {
                                    "content" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "lang" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "last" : {
                                "type" : "date"
                            },
                            "urls" : {
                                "properties" : {
                                    "content" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "lang" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "cancelled" : {
                        "type" : "boolean"
                    },
                    "distance" : {
                        "properties" : {
                            "mandatory" : {
                                "type" : "long"
                            },
                            "mandatoryNet" : {
                                "type" : "long"
                            },
                            "optional" : {
                                "type" : "long"
                            },
                            "optionalNet" : {
                                "type" : "long"
                            }
                        }
                    },
                    "education" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "execution" : {
                        "properties" : {
                            "condition" : {
                                "type" : "long"
                            },
                            "end" : {
                                "type" : "date"
                            },
                            "start" : {
                                "type" : "date"
                            }
                        }
                    },
                    "expires" : {
                        "type" : "date"
                    },
                    "extension" : {
                        "properties" : {
                            "admissionDetails" : {
                                "properties" : {
                                    "credentialRatingModel" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "credentialRatingModelUseNoModel" : {
                                        "type" : "boolean"
                                    },
                                    "eligibilityModelSB" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "eligibilityModelSBUseNoModel" : {
                                        "type" : "boolean"
                                    },
                                    "selectionModel" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "applicationDetails" : {
                                "properties" : {
                                    "applicationModel" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "onlyAsPartOfProgram" : {
                                        "type" : "boolean"
                                    },
                                    "visibleToInternationalApplicants" : {
                                        "type" : "boolean"
                                    },
                                    "visibleToSwedishApplicants" : {
                                        "type" : "boolean"
                                    }
                                }
                            },
                            "department" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "eligibilityExemption" : {
                                "type" : "boolean"
                            },
                            "formOfFunding" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "id" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "itdistance" : {
                                "type" : "boolean"
                            },
                            "keywords" : {
                                "properties" : {
                                    "content" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "lang" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    }
                                }
                            },
                            "startPeriod" : {
                                "properties" : {
                                    "periodNumber" : {
                                        "type" : "long"
                                    },
                                    "vacation" : {
                                        "type" : "text",
                                        "fields" : {
                                            "keyword" : {
                                                "type" : "keyword",
                                                "ignore_above" : 256
                                            }
                                        }
                                    },
                                    "week" : {
                                        "type" : "long"
                                    },
                                    "year" : {
                                        "type" : "long"
                                    }
                                }
                            },
                            "stopWeek" : {
                                "type" : "long"
                            },
                            "tuitionFee" : {
                                "properties" : {
                                    "content" : {
                                        "type" : "boolean"
                                    },
                                    "first" : {
                                        "type" : "long"
                                    },
                                    "total" : {
                                        "type" : "long"
                                    }
                                }
                            },
                            "type" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "uniqueIdentifier" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "fee" : {
                        "properties" : {
                            "amount" : {
                                "type" : "long"
                            },
                            "currency" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "identifier" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "languageOfInstruction" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "lastEdited" : {
                        "type" : "date"
                    },
                    "locations" : {
                        "properties" : {
                            "country" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "municipalityCode" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "postCode" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "streetAddress" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "town" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "paceOfStudyPercentage" : {
                        "type" : "long"
                    },
                    "provider" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    },
                    "timeOfStudy" : {
                        "properties" : {
                            "code" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "type" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "urls" : {
                        "properties" : {
                            "content" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "lang" : {
                                "type" : "text",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "id" : {
                "type" : "keyword"
            },
            "providerSummary" : {
                "properties" : {
                    "providers" : {
                        "type" : "text",
                        "fields" : {
                            "keyword" : {
                                "type" : "keyword",
                                "ignore_above" : 256
                            }
                        }
                    }
                }
            },
            "text_enrichments_results" : {
                "properties" : {
                    "enriched_candidates" : {
                        "properties" : {
                            "competencies" : {
                                "type" : "text",
                                "analyzer": "char_filter_analyzer",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "geos" : {
                                "type" : "text",
                                "analyzer": "char_filter_analyzer",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "occupations" : {
                                "type" : "text",
                                "analyzer": "char_filter_analyzer",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "traits" : {
                                "type" : "text",
                                "analyzer": "char_filter_analyzer",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    },
                    "enriched_candidates_complete" : {
                        "properties" : {
                            "competencies" : {
                                "type" : "text",
                                "analyzer": "char_filter_analyzer",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "geos" : {
                                "type" : "text",
                                "analyzer": "char_filter_analyzer",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "occupations" : {
                                "type" : "text",
                                "analyzer": "char_filter_analyzer",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            },
                            "traits" : {
                                "type" : "text",
                                "analyzer": "char_filter_analyzer",
                                "fields" : {
                                    "keyword" : {
                                        "type" : "keyword",
                                        "ignore_above" : 256
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "timestamp" : {
                "type" : "long"
            }
        }
    }
}
