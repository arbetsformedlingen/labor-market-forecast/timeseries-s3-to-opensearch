import logging

from timeseriestoopensearch.opensearch_store import OpensearchStore

from timeseriestoopensearch import settings
from timeseriestoopensearch.minio_downloader import MinioDownloader

OCCUPATIONS_BATCH_SIZE = 100

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


def opensearch_import():
    log.info('Starting to import educations from Minio to Opensearch..')
    minio_downloader = MinioDownloader()

    best_match_file = minio_downloader.find_best_match_file()

    opensearch_store = OpensearchStore()
    # index_name = opensearch_store.start_new_save(settings.ES_EDUCATIONS_ALIAS,
    #                                              mappings=settings.MINIO_TO_OPENSEARCH_MAPPINGS)
    index_name = opensearch_store.start_new_save(settings.ES_OCCUPATION_PREDICTIONS_ALIAS)

    occupation_counter = 0
    occupations_batch = []
    for occupation in minio_downloader.download_occupations(best_match_file):
        occupation_counter += 1
        occupations_batch.append(occupation)

        if len(occupations_batch) == OCCUPATIONS_BATCH_SIZE:
            import_batch(occupations_batch, opensearch_store, idkey='occupation')
            occupations_batch = []
    # Import remaining educations
    if len(occupations_batch) > 0:
        import_batch(occupations_batch, opensearch_store)

    opensearch_store.create_or_update_alias_for_index(index_name, settings.ES_OCCUPATION_PREDICTIONS_ALIAS)
    log.info(f'Stored {occupation_counter} educations in Opensearch.')


def import_batch(occupations_batch, opensearch_store, idkey='id'):
    try:
        opensearch_store.save_items_in_repository(occupations_batch, idkey)
    except Exception as e:
        log.error(f"Error when saving to opensearch {e}")
