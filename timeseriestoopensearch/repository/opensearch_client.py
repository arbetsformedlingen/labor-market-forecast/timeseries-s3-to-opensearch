import logging

from opensearchpy import OpenSearch
from opensearchpy.helpers import bulk, scan
from timeseriestoopensearch import settings
import time
from datetime import datetime

log = logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

auth = (settings.ES_USER, settings.ES_PWD) # For testing only. Don't store credentials in code.
#ca_certs_path = '/full/path/to/root-ca.pem' # Provide a CA bundle if you use intermediate CAs with your root CA.

log.info(f"Connecting to opensearch host {settings.ES_HOST} {settings.ES_PORT} with user {settings.ES_USER} use_ssl = {settings.ES_USE_SSL} verify certs = {settings.ES_VERIFY_CERTS}")
_default_os_client = OpenSearch(
    hosts = [{'host': settings.ES_HOST, 'port': settings.ES_PORT}],
    http_compress = True, # enables gzip compression for request bodies
    http_auth = auth,
    # client_cert = client_cert_path,
    # client_key = client_key_path,
    use_ssl = settings.ES_USE_SSL,
    verify_certs = settings.ES_VERIFY_CERTS,
    ssl_assert_hostname = False,
    ssl_show_warn = False,
    timeout=30,
    max_retries=10,
    retry_on_timeout=True,
)

def _bulk_generator(documents, indexname, idkey):
    log.debug(f"(_bulk_generator) index: {indexname}, idkey: {idkey}")
    for document in documents:
        doc_id = create_doc_id(document, idkey)

        yield {
            '_index': indexname,
            '_id': doc_id,
            '_source': document
        }

def search(query_dsl, indexname, os_client=None):
    client = pick_os_client(os_client)
    return client.search(index=indexname, body=query_dsl)


def pick_os_client(os_client):
    if os_client:
        client = os_client
    else:
        client = _default_os_client
    return client


def scan_search(query_dsl, indexname, os_client=None):
    client = pick_os_client(os_client)
    return scan(client, query_dsl, index=indexname)

def index_document(document, indexname, idkey='id', os_client=None):
    client = pick_os_client(os_client)
    doc_id = create_doc_id(document, idkey)
    result = None
    try:
        result =  client.index(index=indexname, id=doc_id, body=document)
    except Exception as e:
        log.error(e)
    return result

def create_doc_id(document, idkey):
    doc_id = '-'.join([document[key] for key in idkey]) \
        if isinstance(idkey, list) else document[idkey]
    return doc_id


def bulk_index(documents, indexname, idkey='id', os_client=None):
    client = pick_os_client(os_client)
    action_iterator = _bulk_generator(documents, indexname, idkey)

    result = bulk(client, action_iterator, request_timeout=60, raise_on_error=True, yield_ok=False)
    log.info(f"(bulk_index) result: {result[0]}")
    return result[0]


def index_exists(indexname, os_client=None):
    client = pick_os_client(os_client)
    es_available = False
    fail_count = 0
    while not es_available:
        try:
            result = client.indices.exists(index=[indexname])
            es_available = True
            return result
        except Exception as e:
            if fail_count > 1:
                # Elastic has its own failure management, so > 1 is enough.
                log.error(f"Opensearch not available after try: {fail_count}. Stop trying. {e}")
                raise e
            fail_count += 1
            log.warning(f"Connection fail: {fail_count} for index: {indexname} with {e}")
            time.sleep(1)

def alias_exists(aliasname, os_client=None):
    client = pick_os_client(os_client)
    return client.indices.exists_alias(name=[aliasname])


def get_alias(aliasname, os_client=None):
    client = pick_os_client(os_client)
    return client.indices.get_alias(name=[aliasname])


def put_alias(indexlist, aliasname, os_client=None):
    client = pick_os_client(os_client)
    return client.indices.put_alias(index=indexlist, name=aliasname)


def setup_index(timestamp, index_name_affix, es_mapping, os_client=None):
    log.info(f'Setting up index, index_name_affix: {index_name_affix}')
    if timestamp is None:
        timestamp = datetime.now().strftime('%Y%m%d-%H.%M')
    final_index_name = format_index_name(index_name_affix, timestamp)
    log.info(f'Setting up index with final name: {final_index_name}')

    create_index(final_index_name, es_mapping, os_client=os_client)
    return final_index_name


def format_index_name(index_name_affix, timestamp):
    formatted_index_name = f"{index_name_affix}-{timestamp}"
    return formatted_index_name


def create_index(indexname, extra_mappings=None, os_client=None):
    if index_exists(indexname, os_client=os_client):
        log.info(f"Index {indexname} already exists ")
        return

    client = pick_os_client(os_client)

    log.info(f"Creating index: {indexname}")

    basic_body = {
        "mappings": {
            "properties": {
                "timestamp": {
                    "type": "long"
                },
            }
        }
    }

    if extra_mappings:
        body = extra_mappings
        if 'mappings' in body:
            body.get('mappings', {}).get('properties', {})['timestamp'] = {'type': 'long'}
        else:
            body.update(basic_body)
    else:
        body = basic_body

    result = client.indices.create(index=indexname, body=body, wait_for_active_shards=1)
    if 'error' in result:
        log.error(f"Error: {result} on create index: {indexname} , mapping: {body}")
    else:
        log.info(f"New index created: {indexname} , mapping: {body}")


def add_indices_to_alias(indexlist, aliasname, os_client=None):
    client = pick_os_client(os_client)
    actions = {
        "actions": [
            {"add": {"indices": indexlist, "alias": aliasname}}
        ]
    }
    response = client.indices.update_aliases(body=actions)
    log.info(f"Added: {actions}")
    return response


def update_alias(indexnames, old_indexlist, aliasname, os_client=None):
    client = pick_os_client(os_client)
    actions = {
        "actions": []
    }
    for index in old_indexlist:
        actions["actions"].append({"remove": {"index": index, "alias": aliasname}})

    actions["actions"].append({"add": {"indices": indexnames, "alias": aliasname}})
    client.indices.update_aliases(body=actions)
    log.info(f"Updated: {actions}")

